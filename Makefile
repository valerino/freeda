# multi-arch makefile
# freedah
# jrf, 2017

# source folder (must be first line!!!)
SRC_FOLDER = ./src
export NDK_PATH=$(shell  which ndk-build | xargs dirname )

# mandatory!!!
include global.mk

# compile target
TARGET = freedah

# set ldflags
LDFLAGS += -Llibs/$(MAKECMDGOALS) -Lsrc/libogg/$(BUILD_FOLDER)/$(MAKECMDGOALS) -Lsrc/libspeex/$(BUILD_FOLDER)/$(MAKECMDGOALS) -lstdc++ -lm -lfrida-core -logg -lspeex

# includes are in libs folder
CFLAGS += -std=c++11 -Ilibs/$(MAKECMDGOALS) -Isrc/libogg/include -Isrc/libspeex/include

ifeq ($(DEBUG), 1)
	UGLIFY=cp /tmp/tmp.js /tmp/voiphk.js
else
	UGLIFY=uglifyjs --compress drop_console,dead_code --mangle toplevel,eval -- /tmp/tmp.js > /tmp/voiphk.js
endif

define _build_support_files
	# generate full js
	cp $(SRC_FOLDER)/js/common.js /tmp/common.js;\
	cp $(1) /tmp/hk.js;\
	cat /tmp/common.js /tmp/hk.js > /tmp/tmp.js; \
	$(UGLIFY);\
	cp /tmp/voiphk.js $(BUILD_FOLDER)/$(MAKECMDGOALS)/voiphk.js;\
	# copy apps json
	cp $(SRC_FOLDER)/js/apps.json $(BUILD_FOLDER)/$(MAKECMDGOALS)
endef

clean: _clean
	# clean libs
	cd src/libogg;\
	make clean;\
	cd -;\
	cd src/libspeex;\
	make clean;\
	cd -

_build_js_android:
	# generate full script for android
	$(call _build_support_files,$(SRC_FOLDER)/js/android-voiphk.js)

_build_js_ios:
	# generate full script for ios
	$(call _build_support_files,$(SRC_FOLDER)/js/ios-voiphk.js)

_build_js_osx:
	# generate full script for osx
	$(call _build_support_files,$(SRC_FOLDER)/js/osx-voiphk.js)

_build_libs:
	# build libs
	cd src/libogg;\
	make $(MAKECMDGOALS);\
	cd -;\
	cd src/libspeex;\
	make $(MAKECMDGOALS);\
	cd -

ios-arm: _build_libs _ios-arm _build _ios-codesign _build_js_ios _copy_to_device

ios-arm64: _build_libs _ios-arm64 _build _ios-codesign _build_js_ios _copy_to_device

mac-i386: _build_libs _mac-i386 _build _build_js_osx

mac-x86_64: _build_libs _mac-x86_64 _build _build_js_osx

android-arm: _build_libs _android-arm _build _build_js_android _copy_to_device_android

android-arm64: _build_libs _android-arm64 _build _build_js_android _copy_to_device_android
