#ifndef __DBGH_H__
#define __DBGH_H__

#ifdef __ANDROID_API__
#include <android/log.h>
#endif

#define DEBUG_MESSAGES

// define this to keep debug messages in release versions
// #define DBG_RELEASE_DEBUG_MESSAGES

#ifndef DBG_RELEASE_DEBUG_MESSAGES
#ifdef NDEBUG
#undef DEBUG_MESSAGES
#endif
#endif

#if defined(DEBUG_MESSAGES)
#ifdef __ANDROID_API__
#define DBG_OUT(fmt, args...) \
	__android_log_print(ANDROID_LOG_VERBOSE, "VDBG", "DEBUG: PID-%d:%s:%d:%s(): " fmt, getpid(), __FILE__, __LINE__, __func__, ##args);
#else
#define DBG_OUT(fmt, args...)                                                               \
	fprintf(stderr, "DEBUG: PID-%d:%s:%d:%s(): " fmt, getpid(), __FILE__, __LINE__, __func__, \
					##args)
#endif
#else
#define DBG_OUT(fmt, args...)
#endif

#endif // DBGH_H
