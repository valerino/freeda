/*!
 * unix version of windows's vxstrings.h
 * wolf/ht/2k17
 *
 * usage:
 * at the top of the cpp:
 * #include "vxstrings_unx.h"
 * USE_VXSTR(an_unique_string_for_each_cpp)
 *
 * then in the code:
 * strcpy (dest, _VSTRX("hello!");
 *
 * #define MYSTRING "hello"
 * strcpy (dest, _VSTRX(MYSTRING));
 *
 * NOTE: DO NOT USE THE SAME USE_VXSTR(...) IN MORE THAN A CPP!
 *
 * NOTE: using as below DO NOT WORK !
 * const char str[] = _VSTRX("hello")
 *
 * NOTE: use with caution, since code may become huge (decrypter functions are forced inline for EVERY string)..... but this adds chaos for reversers :)
 */

#ifndef __vxstring_unx__
#define __vxstring_unx__
#define __STDC_WANT_LIB_EXT1__ 1
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <array>
#include <cstddef>
#include <functional>

/**
 * this is a memset_s() implementation for the android ndk (missing in the android stdlib)
 * @param  always_inline [description]
 * @return               [description]
 */
#ifdef __ANDROID_API__
__attribute__((always_inline)) int memset_s(void *v, size_t smax, int c, size_t n) {
	if (v == NULL)
		return EINVAL;
	if (n > smax)
		return EINVAL;

	volatile unsigned char *p = (unsigned char *)v;
	while (smax-- && n--) {
		*p++ = c;
	}

	return 0;
}
#endif

/*
encrypter
*/
template <std::size_t STRSIZE, std::size_t I, std::size_t strid>
class replace_char_t {
public:
	replace_char_t() {}
	__attribute__((always_inline)) void operator()(const char (&src)[STRSIZE], char *dst) {
		if ((STRSIZE % 2) == 0) {
			// use set1 encryptors
			if ((strid % 2) == 0) {
				dst[I] = src[I] ^ ((((size_t)((STRSIZE + 1) * (strid + 15)))) & 0xFF);
			} else {
				dst[I] = src[I] + ((((size_t)((STRSIZE + 2) ^ (strid + 16)) + STRSIZE)) & 0xFF);
			}
		} else {
			// use set2 encryptors
			if ((strid % 2) == 0) {
				dst[I] = src[I] ^ ((((size_t)((STRSIZE + 3) ^ (strid + 17)) * (STRSIZE + 1))) & 0xFF);
			} else {
				dst[I] = src[I] - ((((size_t)((STRSIZE + 4) + (strid + 18)) ^ (STRSIZE + 11))) & 0xFF);
			}
		}
		replace_char_t<STRSIZE, I + 1, strid>()(src, dst);
	}
};

template <std::size_t STRSIZE, std::size_t strid>
class replace_char_t<STRSIZE, STRSIZE, strid> {
public:
	replace_char_t() {}
	__attribute__((always_inline)) void operator()(const char (&src)[STRSIZE], char *dst) {}
};

template <std::size_t STRSIZE, std::size_t strid>
__attribute__((always_inline)) void replace_chars(const char (&src)[STRSIZE], char *dst) {
	replace_char_t<STRSIZE, 0, strid>()(src, dst);
}

/*
decrypter
*/
#define USE_VXSTR(__seed)                                                                            \
	template <std::size_t iidx>                                                                        \
	struct _xstr_##__seed {                                                                            \
		template <std::size_t STRSIZE>                                                                   \
		__attribute__((always_inline)) _xstr_##__seed(const char (&str)[STRSIZE]) {                      \
			static char __tmp__[STRSIZE];                                                                  \
			__the_string__ = __tmp__;                                                                      \
			__str_size__ = STRSIZE;                                                                        \
			replace_chars<STRSIZE, iidx>(str, __the_string__);                                             \
		}                                                                                                \
		~_xstr_##__seed() {                                                                              \
			memset_s(__the_string__, __str_size__ * sizeof(char), 0, __str_size__ * sizeof(char));         \
		}                                                                                                \
		__attribute__((noinline)) const char *decrypter(char *str, size_t STRSIZE, size_t strid) const { \
			auto fn1 = [&](size_t i) -> void {                                                             \
				str[i] = str[i] ^ ((((size_t)((STRSIZE + 1) * (strid + 15)))) & 0xFF);                       \
			};                                                                                             \
			auto fn2 = [&](size_t i) -> void {                                                             \
				str[i] = str[i] - ((((size_t)((STRSIZE + 2) ^ (strid + 16)) + STRSIZE)) & 0xFF);             \
			};                                                                                             \
			auto fn3 = [&](size_t i) -> void {                                                             \
																																																		 \
				str[i] = str[i] ^ ((((size_t)((STRSIZE + 3) ^ (strid + 17)) * (STRSIZE + 1))) & 0xFF);       \
			};                                                                                             \
			auto fn4 = [&](size_t i) -> void {                                                             \
				str[i] = str[i] + ((((size_t)((STRSIZE + 4) + (strid + 18)) ^ (STRSIZE + 11))) & 0xFF);      \
			};                                                                                             \
																																																		 \
			std::function<void(size_t)> set1[2] = {fn1, fn2};                                              \
			std::function<void(size_t)> set2[2] = {fn3, fn4};                                              \
			for (size_t i = 0; i < STRSIZE; ++i) {                                                         \
				if ((STRSIZE % 2) == 0) {                                                                    \
					set1[strid % 2](i);                                                                        \
				} else {                                                                                     \
					set2[strid % 2](i);                                                                        \
				}                                                                                            \
			}                                                                                              \
			return (str);                                                                                  \
		}                                                                                                \
		const char *content() const {                                                                    \
			return decrypter(__the_string__, __str_size__, iidx);                                          \
		}                                                                                                \
		operator const char *() const { return content(); }                                              \
																																																		 \
	private:                                                                                           \
		char *__the_string__;                                                                            \
		std::size_t __str_size__;                                                                        \
	};                                                                                                 \
	template <std::size_t iidx>                                                                        \
	using _x_str = _xstr_##__seed<iidx>;

#ifdef NDEBUG
#define _VSTRX(__x__) _x_str<__COUNTER__>(__x__).content()
#else
#define _VSTRX(__x__) __x__
#endif

#ifdef DISABLE_VXSTR
#define _VSTRX(__x__) __x__
#endif
#endif // #define __vxstring__unx__
