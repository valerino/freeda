Java.perform(function () {
	var ArrayList = Java.use("java.util.ArrayList");

	// we need these classes from the com.android.settings app
	var AppAdapter = Java.use("com.android.settings.applications.ManageApplications$ApplicationsAdapter");
	var AppEntry = Java.use("com.android.settingslib.applications.ApplicationsState$AppEntry");

	// hook the onRebuildComplete method of the ApplicationsAdapter inner class of com.android.settings.applications.ManageApplications
	AppAdapter.onRebuildComplete.overload("java.util.ArrayList").implementation = function (l) {
		 // we are passed the AppEntry list, parse it
		 var appList = Java.cast(l, ArrayList);
		 var size = appList.size();
		 for (i=0;i<size;i++) {
			 // get app name
			 var app = Java.cast(appList.get(i), AppEntry);
			 // sample hiding 'SuperSU'
			 if (app.label['value'] == 'SuperSU') {
				// hide app returning a null string
				app.label['value'] = '';

				// debug msg
				send ('Hiding app!!!')
			 }
		}
		this.onRebuildComplete(l);
	 };
});
