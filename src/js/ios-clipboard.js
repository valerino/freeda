/**
 * hook pasteboard on ios (works only in some apps at the moment, i.e. Safari)
 * wolf/ht/2k17
 */
var pasteboardSetValue = ObjC.classes.UIPasteboard["- setValue:forPasteboardType:"];
Interceptor.attach(pasteboardSetValue.implementation, {
	onEnter: function(args) {
		var message = ObjC.Object(args[2]);
		var type = ObjC.Object(args[3]);
		if (message != null && type != null) {
			// got text, send to freedah!
			var dict = {}
			dict['type'] = 'clipboard';
			dict['data'] = message.toString();
			send(dict);
		}
	}
});
