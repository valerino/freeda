/**
 * hook application launch into Springboard
 * wolf/ht/2k17
 */
var kill = ObjC.classes.SBApplication["- didExitWithContext:"];
Interceptor.attach(kill.implementation, {
	onEnter: function(args) {
		var SBApplication = ObjC.Object(args[0])
		var displayName = SBApplication.displayName().toString();
		var bundle = SBApplication.bundleIdentifier().toString();
		var path = SBApplication.path().toString();

		// send to c layer
		var dict = {}
		dict['type'] = 'app';
		dict['data'] = [displayName, bundle, path, 1];
		send(dict);
	}
});

var launch = ObjC.classes.SBApplication["- processDidLaunch:"];
Interceptor.attach(launch.implementation, {
	onEnter: function(args) {
		var SBApplication = ObjC.Object(args[0])
		var displayName = SBApplication.displayName().toString();;
		var bundle = SBApplication.bundleIdentifier().toString();;
		var path = SBApplication.path().toString();;

		// send to c layer
		var dict = {}
		dict['type'] = 'app';
		dict['data'] = [displayName, bundle, path, 0];
		send(dict);
	}
});
