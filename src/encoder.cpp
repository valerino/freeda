#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <ogg/ogg.h>
#include <speex/speex.h>
#include <speex/speex_header.h>
#include <speex/speex_stereo.h>
#include "dbgh.h"

#define MAX_FRAME_SIZE 2000
#define MAX_FRAME_BYTES 2000

/**
 * the whole code here is borrowed from speexenc sample in the speex (1.2) sources
 */

#if !defined(__LITTLE_ENDIAN__) && (defined(WORDS_BIGENDIAN) || defined(__BIG_ENDIAN__))
#define le_short(s) ((short)((unsigned short)(s) << 8) | ((unsigned short)(s) >> 8))
#define be_short(s) ((short)(s))
#else
#define le_short(s) ((short)(s))
#define be_short(s) ((short)((unsigned short)(s) << 8) | ((unsigned short)(s) >> 8))
#endif

/*
	Comments will be stored in the Vorbis style.
	It is describled in the "Structure" section of
		 http://www.xiph.org/ogg/vorbis/doc/v-comment.html

 The comment header is decoded as follows:
	 1) [vendor_length] = read an unsigned integer of 32 bits
	 2) [vendor_string] = read a UTF-8 vector as [vendor_length] octets
	 3) [user_comment_list_length] = read an unsigned integer of 32 bits
	 4) iterate [user_comment_list_length] times {
			5) [length] = read an unsigned integer of 32 bits
			6) this iteration's user comment = read a UTF-8 vector as [length] octets
			}
	 7) [framing_bit] = read a single bit as boolean
	 8) if ( [framing_bit]  unset or end of packet ) then ERROR
	 9) done.

	 If you have troubles, please write to ymnk@jcraft.com.
	*/

#define readint(buf, base) (((buf[base + 3] << 24) & 0xff000000) | \
														((buf[base + 2] << 16) & 0xff0000) |   \
														((buf[base + 1] << 8) & 0xff00) |      \
														(buf[base] & 0xff))
#define writeint(buf, base, val)          \
	do {                                    \
		buf[base + 3] = ((val) >> 24) & 0xff; \
		buf[base + 2] = ((val) >> 16) & 0xff; \
		buf[base + 1] = ((val) >> 8) & 0xff;  \
		buf[base] = (val)&0xff;               \
	} while (0)

bool _comment_init(char **comments, int *length, char *vendor_string) {
	int vendor_length = strlen(vendor_string);
	int user_comment_list_length = 0;
	int len = 4 + vendor_length + 4;
	char *p = (char *)malloc(len);
	if (p == NULL) {
		DBG_OUT("out of memory\n");
		return false;
	}
	writeint(p, 0, vendor_length);
	memcpy(p + 4, vendor_string, vendor_length);
	writeint(p, 4 + vendor_length, user_comment_list_length);
	*length = len;
	*comments = p;
	return true;
}

/*Write an Ogg page to a file pointer*/
int _oe_write_page(ogg_page *page, FILE *fp) {
	int written;
	written = fwrite(page->header, 1, page->header_len, fp);
	written += fwrite(page->body, 1, page->body_len, fp);

	return written;
}

/* Convert input audio bits, endians and channels */
int _read_samples(FILE *fin, int frame_size, int bits, int channels, int lsb, short *input, char *buff, spx_int32_t *size) {
	unsigned char in[MAX_FRAME_BYTES * 2] = {0};
	int i = 0;
	short *s = NULL;
	int nb_read = 0;

	if (size && *size <= 0) {
		return 0;
	}
	/*Read input audio*/
	if (size)
		*size -= bits / 8 * channels * frame_size;
	if (buff) {
		for (i = 0; i < 12; i++)
			in[i] = buff[i];
		nb_read = fread(in + 12, 1, bits / 8 * channels * frame_size - 12, fin) + 12;
		if (size)
			*size += 12;
	} else {
		nb_read = fread(in, 1, bits / 8 * channels * frame_size, fin);
	}
	nb_read /= bits / 8 * channels;

	/*fprintf (stderr, "%d\n", nb_read);*/
	if (nb_read == 0)
		return 0;

	s = (short *)in;
	if (bits == 8) {
		/* Convert 8->16 bits */
		for (i = frame_size * channels - 1; i >= 0; i--) {
			s[i] = (in[i] << 8) ^ 0x8000;
		}
	} else {
		/* convert to our endian format */
		for (i = 0; i < frame_size * channels; i++) {
			if (lsb)
				s[i] = le_short(s[i]);
			else
				s[i] = be_short(s[i]);
		}
	}

	/* FIXME: This is probably redundent now */
	/* copy to float input buffer */
	for (i = 0; i < frame_size * channels; i++) {
		input[i] = (short)s[i];
	}

	for (i = nb_read * channels; i < frame_size * channels; i++) {
		input[i] = 0;
	}

	return nb_read;
}

int rawToOgg(const char *inRawPath, const char *outOggPath, int inSampleRate, int inBits, int inChannels) {
	int ok = -1;
	DBG_OUT("in: %s, out: %s, smp:%d, bits:%d, chan:%d\n", inRawPath, outOggPath, inSampleRate, inBits, inChannels);

	// start speex/ogg compression (we have everything we need for speex: samplerate, framecount, channels, format(bits) )
	spx_int32_t rate = inSampleRate;
	int chan = inChannels;
	int fmt = inBits;
	int lsb = 1; // TODO: is input always little endian ?!?!?

	ogg_page og = {0};
	ogg_packet op = {0};
	ogg_stream_state os = {0};
	FILE *fin = NULL;
	FILE *fout = NULL;
	char first_bytes[12] = {0};
	const SpeexMode *mode = NULL;
	SpeexHeader header = {0};
	void *st = NULL;
	short input[MAX_FRAME_SIZE] = {0};
	spx_int32_t frame_size = 0;
	spx_int32_t tmp = 0;
	SpeexBits bits = {0};
	char cbits[MAX_FRAME_BYTES] = {0};
	int nb_samples;
	int total_samples = 0;
	int nb_encoded = 0;
	int nbBytes = 0;
	spx_int32_t lookahead = 0;
	int bytes_written = 0;
	int ret = 0;
	int result = 0;
	char *comments = NULL;
	char vendor_string[] = " ";
	int comments_length = 0;
	int packet_size = 0;
	int eos = 0;
	int id = -1;

	int modeId;                 // speex mode, based on input
	int vbr_enabled = 0;        // use vbr encoding
	int nframes = 1;            // frames x ogg packet (1-10, default 1)
	spx_int32_t complexity = 3; // encoding complexity (0-10, default 3)
	spx_int32_t quality = 5;    // encoding quality (1-10, default 5)
	float vbr_quality = (float)quality;
	if (rate <= 16000) {
		modeId = SPEEX_MODEID_NB;
	} else if (rate > 16000 && rate <= 32000) {
		modeId = SPEEX_MODEID_WB;
	} else if (rate > 32000 && rate <= 48000) {
		modeId = SPEEX_MODEID_UWB;
	} else {
		// TODO: downsample ? never seen voip streams above 48khz ....
		DBG_OUT("input sample rate too high for %s (%d), need downsampling first\n", inRawPath, inSampleRate);
		goto __exit;
	}

	// initialize comments header (empty)
	if (!_comment_init(&comments, &comments_length, vendor_string)) {
		goto __exit;
	}

	// initialize Ogg stream struct
	if (ogg_stream_init(&os, rand()) == -1) {
		DBG_OUT("ogg_stream_init()\n");
		goto __exit;
	}

	// open input file
	fin = fopen(inRawPath, "rb");
	if (!fin) {
		DBG_OUT("can't open IN file: %s\n", inRawPath);
		goto __exit;
	}
	fread(first_bytes, 1, 12, fin);

	// setup speex
	mode = speex_lib_get_mode(modeId);
	speex_init_header(&header, rate, 1, mode);
	header.frames_per_packet = nframes;
	header.vbr = vbr_enabled;
	header.nb_channels = chan;
	st = speex_encoder_init(mode);
	fout = fopen(outOggPath, "w+b");
	if (!fout) {
		DBG_OUT("can't open OUT file: %s\n", outOggPath);
		goto __exit;
	}

	speex_encoder_ctl(st, SPEEX_GET_FRAME_SIZE, &frame_size);
	speex_encoder_ctl(st, SPEEX_SET_COMPLEXITY, &complexity);
	speex_encoder_ctl(st, SPEEX_SET_SAMPLING_RATE, &rate);

	if (vbr_enabled) {
		tmp = 1;
		speex_encoder_ctl(st, SPEEX_SET_VBR, &tmp);
		speex_encoder_ctl(st, SPEEX_SET_VBR_QUALITY, &vbr_quality);
		speex_encoder_ctl(st, SPEEX_SET_VAD, &tmp);
		speex_encoder_ctl(st, SPEEX_SET_DTX, &tmp);
	} else {
		speex_encoder_ctl(st, SPEEX_SET_QUALITY, &quality);
	}

	speex_encoder_ctl(st, SPEEX_GET_LOOKAHEAD, &lookahead);

	/*Write header*/
	op.packet = (unsigned char *)speex_header_to_packet(&header, &packet_size);
	op.bytes = packet_size;
	op.b_o_s = 1;
	op.e_o_s = 0;
	op.granulepos = 0;
	op.packetno = 0;
	ogg_stream_packetin(&os, &op);
	free(op.packet);

	while ((result = ogg_stream_flush(&os, &og))) {
		if (!result)
			break;
		ret = _oe_write_page(&og, fout);
		if (ret != og.header_len + og.body_len) {
			DBG_OUT("failed writing header 0 to output stream\n");
			goto __exit;
		} else
			bytes_written += ret;
	}

	op.packet = (unsigned char *)comments;
	op.bytes = comments_length;
	op.b_o_s = 0;
	op.e_o_s = 0;
	op.granulepos = 0;
	op.packetno = 1;
	ogg_stream_packetin(&os, &op);

	/* writing the rest of the speex header packets */
	while ((result = ogg_stream_flush(&os, &og))) {
		if (!result)
			break;
		ret = _oe_write_page(&og, fout);
		if (ret != og.header_len + og.body_len) {
			DBG_OUT("failed writing header 1 to output stream\n");
			goto __exit;
		} else
			bytes_written += ret;
	}

	speex_bits_init(&bits);

	nb_samples = _read_samples(fin, frame_size, fmt, chan, lsb, input, first_bytes, NULL);
	if (nb_samples == 0) {
		eos = 1;
	}
	total_samples += nb_samples;
	nb_encoded = -lookahead;

	/*Main encoding loop (one frame per iteration)*/
	while (!eos || total_samples > nb_encoded) {
		id++;
		/*Encode current frame*/
		if (chan == 2) {
			speex_encode_stereo_int(input, frame_size, &bits);
		}
		speex_encode_int(st, input, &bits);

		nb_encoded += frame_size;
		nb_samples = _read_samples(fin, frame_size, fmt, chan, lsb, input, NULL, NULL);
		if (nb_samples == 0) {
			eos = 1;
		}
		if (eos && total_samples <= nb_encoded) {
			op.e_o_s = 1;
		} else {
			op.e_o_s = 0;
		}
		total_samples += nb_samples;

		if ((id + 1) % nframes != 0) {
			continue;
		}
		speex_bits_insert_terminator(&bits);
		nbBytes = speex_bits_write(&bits, cbits, MAX_FRAME_BYTES);
		speex_bits_reset(&bits);
		op.packet = (unsigned char *)cbits;
		op.bytes = nbBytes;
		op.b_o_s = 0;
		/*Is this redundent?*/
		if (eos && total_samples <= nb_encoded) {
			op.e_o_s = 1;
		} else {
			op.e_o_s = 0;
		}
		op.granulepos = (id + 1) * frame_size - lookahead;
		if (op.granulepos > total_samples) {
			op.granulepos = total_samples;
		}
		op.packetno = 2 + id / nframes;
		ogg_stream_packetin(&os, &op);

		/*Write all new pages (most likely 0 or 1)*/
		while (ogg_stream_pageout(&os, &og)) {
			ret = _oe_write_page(&og, fout);
			if (ret != og.header_len + og.body_len) {
				DBG_OUT("failed writing header 2 to output stream\n");
				goto __exit;
			} else
				bytes_written += ret;
		}
	}
	if ((id + 1) % nframes != 0) {
		while ((id + 1) % nframes != 0) {
			id++;
			speex_bits_pack(&bits, 15, 5);
		}
		nbBytes = speex_bits_write(&bits, cbits, MAX_FRAME_BYTES);
		op.packet = (unsigned char *)cbits;
		op.bytes = nbBytes;
		op.b_o_s = 0;
		op.e_o_s = 1;
		op.granulepos = (id + 1) * frame_size - lookahead;
		if (op.granulepos > total_samples) {
			op.granulepos = total_samples;
		}
		op.packetno = 2 + id / nframes;
		ogg_stream_packetin(&os, &op);
	}

	/*Flush all pages left to be written*/
	while (ogg_stream_flush(&os, &og)) {
		ret = _oe_write_page(&og, fout);
		if (ret != og.header_len + og.body_len) {
			DBG_OUT("failed writing header 3 to output stream\n");
			goto __exit;
		} else {
			bytes_written += ret;
		}
	}

	// ok!
	ok = 0;

__exit:
	if (st) {
		speex_encoder_destroy(st);
	}
	speex_bits_destroy(&bits);
	ogg_stream_clear(&os);

	if (comments == NULL) {
		free(comments);
	}
	if (fin) {
		fclose(fin);
	}
	if (fout) {
		fclose(fout);
	}
	if (ok == -1) {
		// delete compressed file if any
		unlink(outOggPath);
	}
	return ok;
}
