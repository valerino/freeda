#ifndef __ENCODER_H__
#define __ENCODER_H__

/**
 * convert raw file to .OGG speex-encoded file
 * @param  inRawPath    [description]
 * @param  outOggPath   [description]
 * @param  inSampleRate [description]
 * @param  inBits       [description]
 * @param  inChannels   [description]
 * @return              0 on success
 */
int rawToOgg(const char *inRawPath, const char *outOggPath, int inSampleRate, int inBits, int inChannels);

#endif
