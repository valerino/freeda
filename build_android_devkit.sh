#!/usr/bin/env sh
if [ $# -le 1 ]; then
	echo "usage: $0 <frida_root> <freeda_root> <--clean>"
	exit 1
fi

__CWD=$(pwd)
if [ "$3" == "--clean" ]; then
	echo ". cleaning frida"
	cd $1
	make clean
	cd $__CWD
	echo ". cleaning libs"
	rm -rf $2/libs/android-arm
	rm -rf $2/libs/android-arm64
fi

echo ". replacing config.mk"
cp $2/frida_android_config.mk $1/config.mk
cp $2/capstone_android_config.mk $1/capstone/config.mk

echo ". building frida core"
cd $1
make core-android

echo ". assembling frida core lib (arm64)"
mkdir -p $2/libs/android-arm64
./releng/devkit.py frida-core android-arm64 ./build/frida-android-arm64
cp ./build/frida-android-arm64/libfrida-core.a $2/libs/android-arm64
cp ./build/frida-android-arm64/frida-core.h $2/libs/android-arm64

echo ". assembling frida core lib (arm)"
mkdir -p $2/libs/android-arm
./releng/devkit.py frida-core android-arm ./build/frida-android-arm
cp ./build/frida-android-arm/libfrida-core.a $2/libs/android-arm
cp ./build/frida-android-arm/frida-core.h $2/libs/android-arm

cd $__CWD

