#!/usr/bin/env bash
VERSION=$1
if [ $# -eq 0 ]; then
echo usage: $0 '<version> (i.e. update_frida.sh 9.1.27, check https://github.com/frida/frida/releases)'
exit 1
fi

declare -a archs=("ios-arm" "ios-arm64" "macos-i386" "macos-x86_64" "android-arm" "android-arm64")
declare -a archs_srv=("ios-arm" "ios-arm64" "macos-universal" "android-arm" "android-arm64")
declare -a archs_gad=("ios-universal" "macos-universal" "android-arm" "android-arm64")

function _download_internal() {
# $1 is 'core', 'gum', server
# $2 is os-arch
# $3 is dest base
# $4 is 1 for server
	echo Downloading\: $1-$2 to\: $3

		if [ $4 -eq 1 ]; then
# server
			curl -L https://github.com/frida/frida/releases/download/$VERSION/frida-$1-$VERSION-$2.xz -o /tmp/tmp.xz
				elif [ $4 -eq 2 ]; then
# gadget
				if [ "$2" == 'mac-universal' ] || [ "$2" == 'ios-universal' ]; then
					curl -L https://github.com/frida/frida/releases/download/$VERSION/frida-$1-$VERSION-$2.dylib.xz -o /tmp/tmp.xz
				else
					curl -L https://github.com/frida/frida/releases/download/$VERSION/frida-$1-$VERSION-$2.so.xz -o /tmp/tmp.xz
						fi
		else
# gum
			curl -L https://github.com/frida/frida/releases/download/$VERSION/frida-$1-devkit-$VERSION-$2.tar.xz -o /tmp/tmp.tar.gz
				fi

				if [ $? -eq 0 ]; then
					mkdir -p $3/$2
						if [ $4 -eq 1 ]; then
# server
							xz -d -c /tmp/tmp.xz > $3/$2/frida-server
								elif [ $4 -eq 2 ]; then
# gadget
								if [ "$2" == 'mac-universal' ] || [ "$2" == 'ios-universal' ]; then
									xz -d -c /tmp/tmp.xz > $3/$2/frida-gadget.dylib
								else
									xz -d -c /tmp/tmp.xz > $3/$2/frida-gadget.so
										fi
						else
# gum
							tar xvfJ /tmp/tmp.tar.gz -C $3/$2
								fi
								fi
}

function download() {
# $1 is 'core', 'gum'
# $2 is dest base
# $3 is 1 for server, 2 for gadget

	if [ $3 -eq 1 ]; then
# server
		for arch in "${archs_srv[@]}"
			do
				_download_internal $1 $arch $2 $3
					done
					elif [ $3 -eq 2 ]; then
# gadget
					for arch in "${archs_gad[@]}"
						do
							_download_internal $1 $arch $2 $3
								done
	else
# gum/core
		for arch in "${archs[@]}"
			do
				_download_internal $1 $arch $2 $3
					done
					fi
}

# download cores
echo 'Downloading FRIDA cores'
download core ./libs 0

#
# download gums
# echo 'Downloading FRIDA gums'
# download gum ./libs 0
#
# download server
echo 'Downloading FRIDA servers'
download server ./bin 1

# download gadgets
# echo 'Downloading FRIDA gadgets'
# download gadget ./bin 2

# done
rm /tmp/tmp.xz
rm /tmp/tmp.tar.gz

exit 0
