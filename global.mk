# to be included by other makefiles, control debug/release builds
# jrf, 2017

# change this to reflect build host
NDK_HOST=darwin-x86_64
NDK_STRIP=$(ANDROID_NDK_ROOT)/toolchains/aarch64-linux-android-4.9/prebuilt/$(NDK_HOST)/bin/aarch64-linux-android-strip 
# to build debug/release
ifeq ($(DEBUG), 1)
	BUILD_FOLDER=./build/debug
	OBJ_FOLDER = ./obj/debug
	_CFLAGS=-g3 -O0 -DDEBUG
else
	BUILD_FOLDER=./build/release
	OBJ_FOLDER = ./obj/release
	_CFLAGS=-Os -DNDEBUG
endif

# disable runtime string encryption ?
ifeq ($(DISABLE_VXSTR), 1)
	_CFLAGS+=-DDISABLE_VXSTR=1
endif
	
# set device destination folder
ifneq (,$(findstring android,$(MAKECMDGOALS)))
	# on android, copy to tmp
	DEVICE_DEST:=/data/local/tmp
else
	# on ios, copy to /private/var/tmp
	DEVICE_DEST:=/private/var/tmp
endif

# to copy built product on target device
COPY_TO_DEVICE_FUNC=ssh root@$(DEVICE_IP) rm $(DEVICE_DEST)/$(TARGET); \
	scp $(BUILD_FOLDER)/$(MAKECMDGOALS)/* root@$(DEVICE_IP):$(DEVICE_DEST)
COPY_TO_DEVICE_FUNC_ANDROID=adb shell su -c rm $(DEVICE_DEST)/$(TARGET); \
	$(NDK_STRIP) --strip-all $(BUILD_FOLDER)/$(MAKECMDGOALS)/$(TARGET); \
	adb push $(BUILD_FOLDER)/$(MAKECMDGOALS)/* $(DEVICE_DEST)

# matches sources and objs
SOURCES += $(wildcard $(SRC_FOLDER)/*.c)
SOURCES += $(wildcard $(SRC_FOLDER)/*.cpp)
OBJECTS += $(SOURCES:$(SRC_FOLDER)/%.c=$(OBJ_FOLDER)/$(MAKECMDGOALS)/%.o)
OBJECTS += $(SOURCES:$(SRC_FOLDER)/%.cpp=$(OBJ_FOLDER)/$(MAKECMDGOALS)/%.o)

# initialize cflags and ldflags
CFLAGS = $(_CFLAGS) -fPIC -Wall -pipe -fvisibility=hidden

# build rule for .c
$(OBJ_FOLDER)/$(MAKECMDGOALS)/%.o : $(SRC_FOLDER)/%.c
	# compile
	$(CC) $(CFLAGS) -c $< -o $@ # $(LDFLAGS)

# build rule for .cpp
$(OBJ_FOLDER)/$(MAKECMDGOALS)/%.o : $(SRC_FOLDER)/%.cpp
	# compile
	$(CC) $(CFLAGS) -c $< -o $@

_build_staticlib: $(OBJECTS)
	# ar
	$(AR) rcs $(BUILD_FOLDER)/$(MAKECMDGOALS)/$(TARGET).a $(OBJ_FOLDER)/$(MAKECMDGOALS)/*.o

_build: $(OBJECTS)
	# link
	$(CC) $(CFLAGS) $(OBJ_FOLDER)/$(MAKECMDGOALS)/*.o -o $(BUILD_FOLDER)/$(MAKECMDGOALS)/$(TARGET) $(LDFLAGS)

all: _usage

# these must be declared phony!
.PHONY: _mk_folders _clean _usage COPY_TO_DEVICE_FUNC _copy_to_device _ios-codesign

# copy target to mobile device
_copy_to_device:
	$(if $(DEVICE_IP),$(call COPY_TO_DEVICE_FUNC))

_copy_to_device_android:
	$(call COPY_TO_DEVICE_FUNC_ANDROID)

_ios-codesign:
	# sign binary for ios
	codesign -v -s 'iPhone Developer' $(BUILD_FOLDER)/$(MAKECMDGOALS)/$(TARGET)

_usage:
	@echo "usage:"
	@echo "make <os-arch> [DEVICE_IP=/ip/to/mobile] [NDK_PATH=/path/to/ndk] [NDK_HOST=darwin-x86_64] [DEBUG=1]"
	@echo "supported os/archs:"
	@echo "ios-arm,ios-arm64,mac-i386,mac-x86_64,android-arm,android-arm64"

_mk_folders:
	mkdir -p $(BUILD_FOLDER)/$(MAKECMDGOALS);\
	mkdir -p $(OBJ_FOLDER)/$(MAKECMDGOALS)

_clean:
	rm -rf $(dir $(BUILD_FOLDER)) || :;\
	rm -rf $(dir $(OBJ_FOLDER)) || :;\

################################
# ios
################################
__ios: _mk_folders
	$(eval CC:=$(shell xcrun --sdk iphoneos --find clang))
	$(eval AR:=$(shell xcrun --sdk iphoneos --find ar))
	$(eval IOS_SDK:=$(shell xcrun --sdk iphoneos --show-sdk-path))
	$(eval IOS_VERSION_MIN:=7.0)
	$(eval CFLAGS+=-DTARGET_OS_IPHONE -fembed-bitcode-marker -miphoneos-version-min=$(IOS_VERSION_MIN) -isysroot $(IOS_SDK))
	$(eval LDFLAGS+=-lbsm -lresolv -Wl,-framework,Foundation,-framework,CoreGraphics,-framework,UIKit,-iphoneos_version_min,$(IOS_VERSION_MIN) -Wl,-dead_strip)

__ios-arm: __ios
	$(eval CFLAGS+=-arch armv7 -mthumb)

__ios-arm64: __ios
	$(eval CFLAGS+=-arch arm64)

_ios-arm: __ios-arm

_ios-arm64: __ios-arm64

################################
# mac
################################
__mac: _mk_folders
	$(eval CC:=$(shell xcrun --sdk macosx --find clang))
	$(eval AR:=$(shell xcrun --sdk macosx --find ar))
	$(eval OSX_SDK:=$(shell xcrun --sdk macosx --show-sdk-path))
	$(eval OSX_VERSION_MIN:=10.9)
	$(eval CFLAGS+=-DTARGET_OS_MAC -mmacosx-version-min=$(OSX_VERSION_MIN) -isysroot $(OSX_SDK))
	$(eval LDFLAGS+=-lbsm -lresolv -Wl,-framework,Foundation,-framework,AppKit,-macosx_version_min,$(OSX_VERSION_MIN),-dead_strip,-no_compact_unwind)

__mac-i386: __mac
	$(eval CFLAGS+=-arch i386)

__mac-x86_64: __mac
	$(eval CFLAGS+=-arch x86_64)

_mac-i386: __mac-i386

_mac-x86_64: __mac-x86_64

################################
# android
################################
__android: _mk_folders
	$(if $(value NDK_PATH),, \
		$(error missing NDK_PATH, must be set to android NDK path!) \
	endif)

	$(eval CC:=$(NDK_PATH)/toolchains/llvm/prebuilt/$(NDK_HOST)/bin/clang)
	$(eval CFLAGS+=-no-canonical-prefixes -fPIE -fvisibility=hidden -fvisibility-inlines-hidden -ffunction-sections -funwind-tables -fno-exceptions -fno-rtti -DANDROID)
	$(eval CFLAGS +=-I$(NDK_PATH)/sources/cxx-stl/llvm-libc++/include -I$(NDK_PATH)/sources/android/support/include)
	$(eval LDFLAGS+=-s -pie -ldl -llog -fuse-ld=gold -Wl,--export-dynamic -Wl,--no-undefined,--gc-sections,-z,noexecstack,-z,relro,-z,now)

__android-arm: __android
	$(eval AR:=$(NDK_PATH)/toolchains/arm-linux-androideabi-4.9/prebuilt/$(NDK_HOST)/bin/arm-linux-androideabi-gcc-ar)
	$(eval CFLAGS+=--sysroot=$(NDK_PATH)/platforms/android-19/arch-arm)
	$(eval CFLAGS+=--gcc-toolchain=$(NDK_PATH)/toolchains/arm-linux-androideabi-4.9/prebuilt/$(NDK_HOST) -I$(NDK_PATH)/platforms/android-19/arch-arm/usr/include)
	$(eval CFLAGS+=--target=armv7-none-linux-androideabi -march=armv7-a -mthumb -mfloat-abi=softfp -mfpu=vfpv3-d16)
	$(eval LDFLAGS+=-Wl,--fix-cortex-a8,--icf=safe -fuse-ld=gold)

__android-arm64: __android
	$(eval AR:=$(NDK_PATH)/toolchains/aarch64-linux-android-4.9/prebuilt/$(NDK_HOST)/bin/aarch64-linux-android-gcc-ar)
	$(eval CFLAGS+=--sysroot=$(NDK_PATH)/platforms/android-21/arch-arm64 --gcc-toolchain=$(NDK_PATH)/toolchains/aarch64-linux-android-4.9/prebuilt/$(NDK_HOST))
	$(eval CFLAGS+=-I$(NDK_PATH)/platforms/android-21/arch-arm64/usr/include)
	$(eval CFLAGS+=--target=aarch64-none-linux-android)

_android-arm: __android-arm

_android-arm64: __android-arm64
