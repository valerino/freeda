# Freeda: frida porting to Makefile for our pleasure


Inject JavaScript to explore native apps on Windows, macOS, Linux, iOS, Android, and QNX(really ??).

## 1. dependencies/prerequisites
~~~
XCode and commandline tools for ios/osx
Android NDK r15c for android (or, the latest one supported by the current frida version)
nodejs and uglify-js for the script obfuscator: npm install uglify-js -g
~~~

1a. initialize the playground first with **_update_frida.sh \<version>_** to download/update the frida core libraries and frida-server.
~~~
./update_frida 9.1.27
(check https://github.com/frida/frida/releases for versions)
~~~

1b. (optionally, but advised to, rebuild frida from scratch using freeda buildscript)
~~~
# example for android
mkdir -p <frida_repo>
cd <frida_repo>
git clone --recursive https://github.com/frida/frida
cd <freeda_repo>
ANDROID_NDK_ROOT=<android_ndk_root> ./build_android_devkit.sh <frida_repo> <freeda_repo> [--clean to rebuild from scratch]
~~~

2. be sure to transfer _**frida-server**_ in **_bin/\<os-arch>_** on the target device and have it running if you intend to use frida tools (for running freedah, it's not needed)!

## 2. build the project (freedah, freeda hooks)
~~~~
# example for android
it clone <this repo>
cd freeda
ANDROID_NDK_ROOT=<android_ndk_root> [env vars] make <os-arch>|clean
~~~~

## 3. make parameters (and passing environment variables)
**os-arch**
~~~
ios-arm, ios-arm64, mac-i386, mac-x86_64, android-arm, android-arm64
~~~
(windows not needed ?)

all the following environment variables may be set via export or directly on the make commandline:

**DEVICE_IP=...**
~~~
is the ip of the mobile device to deploy to, optional (android/ios only).

products are deployed to /private/var/tmp on ios and to /data/local/tmp on android.

~~~

**NDK_PATH=...**
~~~
android only, is the path to the android ndk base directory

(i.e. /opt/android-ndk).
~~~

**NDK_HOST=...**
~~~
android only, host architecture, default is darwin-x86_64
~~~

**DEBUG=1**
~~~
builds debug binaries
~~~

**DISABLE_VXSTR=1**
~~~
disables release build's runtime string encryption (may cause incompatibilities with some compiler ....)
~~~
## Learn more

Have a look at our [documentation](http://www.frida.re/docs/home/).
